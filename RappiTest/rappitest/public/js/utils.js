function txtKeyPress(e) {
    var evtobj = window.event ? event : e;
    var tecla = evtobj.keyCode;
    if (tecla === 13) {
        bsAjaxJson({op: $("#op").val(), entrada: $("#txtEntrada").val()}, function (data, json, hayError) {

        });
    }
}

function bsAjaxJson(params, fSuccess, fErrorJson, mostrarMsj) {
    bsAjax(CONTROLLER, params, fSuccess, null, null, fErrorJson, null, null, mostrarMsj);
}

function bsAjax(url, params, fSuccess, fErrorMetodo, fErrorAjax, fErrorJson, objetoDestino, htmlCargando, mostrarMsj) {
    if (mostrarMsj === null || mostrarMsj === undefined) {
        mostrarMsj = true;
    }

    if (url === null || url === undefined) {
        url = CONTROLLER;
    }
    if (htmlCargando === null || htmlCargando === undefined) {
        htmlCargando = "<i>Cargando...</i>";
    }
    if (fErrorAjax === null || fErrorAjax === undefined) {///Funcion cuando el Metodo no logra la Comunicacion
        funcionErrorAjax = function (error) {
            escribir("Upps, ha ocurrido un error de comunicaci&oacute;n!!!!");
        };
    }
    if (fErrorJson === null || fErrorJson === undefined) {
        ///Funcion cuando hay un error de Parser al convertir la cadena Json recibida
        fErrorJson = function (error) {
            escribir("Upps, hubo un error.....los datos recibidos no son los esperados!!!!");
        };
    }
    if (fSuccess === null || fSuccess === undefined) {
        fSuccess = function () {
        };
    }
    if (fErrorMetodo === null || fErrorMetodo === undefined) {
        if (objetoDestino !== undefined && objetoDestino !== null) {
            fErrorMetodo = function (ret) {
                $("#" + objetoDestino).html(ret.error);
            };
        } else {
            fErrorMetodo = function (ret) {
            };
        }
    }
    fSuccessInterna = function (data) {
        console.log("-->" + data);
        var ret = null;
        try {
            ret = eval("(" + data + ")");//Evaluacion a Json
            escribir(arrayToString(ret.error, "<br>"));
            if (mostrarMsj) {
                escribir(arrayToString(ret.msj));
            }
            fSuccess(data, ret, ret.hasOwnProperty("error"));
            if (ret.hasOwnProperty("op")) {
                $("#op").val(ret.op);
            }
            siguienteEntrada(ret);
        } catch (e) {
            console.log(e);
            fErrorJson(e);
        }
    };

    $.ajax({
        type: "POST",
        url: url,
        data: params,
        cache: false,
        dataType: "html",
        success: fSuccessInterna,
        error: fErrorAjax
    });
}

function arrayHtmlToObjeto(array, idObjeto) {
    $("#" + idObjeto).html(array.join(""));
}

function arrayToString(array, sep) {
    if (array !== null && array !== undefined) {
        if (Array.isArray(array)) {
            return array.join(sep);
        } else {
            return array;
        }
    } else {
        return undefined;
    }
}
function nuloAVacio(valor) {
    if (valor === null || valor === undefined) {
        valor = "";
    }
    return valor;
}