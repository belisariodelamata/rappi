function iniciar() {
    $("#bodyTable").html("");
    bsAjaxJson({op: 1}, function success(respuesta, json, hayError) {
    });
}
function siguienteEntrada(json, hayError) {
    if (json.hasOwnProperty("entrada")) {
        escribir(json.entrada, "txtEntrada");
    }
}
function escribir(texto, idText) {
    if (texto===null || texto===undefined){
        return;
    }
    if (Array.isArray(texto)){
        texto=arrayToString(texto, "<br>");
    }
    if (idText === null || idText === undefined) {
        idText = "";
    }
    if (idText !== "") {
        var anterior = $("#" + idText);
        if (anterior.length > 0) {
            anterior.removeAttr("id");
            anterior.attr("disabled", "disabled");
        }
    }
    var html = new Array();
    html.push("<tr>");
    html.push("<td");
    if (idText === "") {
        html.push(" colspan='2'");
    }
    html.push(">");
    html.push(texto);
    html.push("</td>");
    html.push("<td>");
    if (idText !== "") {
        html.push("<input type='text' ");
        html.push("id='" + idText + "'");
        html.push("value='' />");
    }
    html.push("</td>");
    html.push("</tr>");
    agregarFila(html);
    if (idText !== "") {
        $("#" + idText).keypress(txtKeyPress) ;
        $("#" + idText).focus() ;
    }
}
function agregarFila(html) {
    var h = $(html.join(""));
    $("#bodyTable").append(h);
}


