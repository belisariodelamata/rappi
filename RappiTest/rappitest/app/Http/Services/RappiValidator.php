<?php

/**
 * Description of RappiValidator
 *
 * @author BS
 */
namespace App\Http\Services;

class RappiValidator {
    /**
     * @param type $array
     * @param type $itInicial indice inicial de verificacion
     * @return boolean Verdadero si todos los valores de las posiciones 
     * evaluadas corresponden a un numero
     */
    public static function validarNumero($array, $itInicial){
        for($it=$itInicial; $it<count($array);$it++){
            if (!is_numeric($array[$it])){
                return false;
            }
        }
        return true;
    }
}
