<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Test
 *
 * @author BS
 */
namespace App\Http\Bean;

class Test {
    //put your code here
    var $matriz=array();
    var $sentencias=array();
    
    public function __construct($num){
        for($i=0; $i<$num; $i++){
            for($j=0; $j<$num; $j++){
                for($k=0; $k<$num; $k++){
                    $this->matriz[$i][$j][$k]=0;
                }
            }
        }
    }
    public function agregarSentencia($sentencia){
        $this->sentencias[]=$sentencia;
    }
    public function getVistaMatriz(){
        $m=  var_export($this->matriz, true);
        return $m;
    }    
    public function actualizar($x, $y, $z, $w){
        $this->matriz[$x-1][$y-1][$z-1]=$w;
        return true;
}
    public function consulta($x1, $y1, $z1, $x2, $y2, $z2 ){
        if ($x2<$x1){
            $tmp=$x1;
            $x1=$x2;
            $x2=$tmp;
        }
        if ($y2<$y1){
            $tmp=$y1;
            $y1=$y2;
            $y2=$tmp;
        }
        if ($z2<$z1){
            $tmp=$z1;
            $z1=$z2;
            $z2=$tmp;
        }
        $suma=0;
        for($i=$x1;$i<=$x2;$i++){
            for($j=$y1;$j<=$y2;$j++){
                for($k=$z1;$k<=$z2;$k++){
                    $suma+=$this->matriz[$i-1][$j-1][$k-1];
                }            
            }            
        }
        return $suma;
    }
    public function ejecutar($sentencia){
        //partes de Sentencia
        $ps = explode(" ", $sentencia);
        if ($ps[0]=="QUERY"){
            return $this->consulta($ps[1], $ps[2], $ps[3], $ps[4],$ps[5],$ps[6]);
        }else
        if ($ps[0]=="UPDATE"){
            return $this->actualizar($ps[1], $ps[2], $ps[3], $ps[4]);
        }
        return null;
    }
    public function ejecutarUltimaSentencia(){
        return $this->ejecutar($this->sentencias[count($this->sentencias)-1]);
    }
    
}
