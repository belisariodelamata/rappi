<?php
/**
 * Configuracion del Test
 *
 * @author BS
 */
namespace App\Http\Bean;


class ConfTest {
    var $numeroTest=0;
    var $tamanoMatriz=0;
    var $numOperaciones=0;
    var $itTest=0;
    var $itOperacion=0;
    
    var $test=array();
    public function setNumeroTest($numeroTest){
        $this->numeroTest=$numeroTest;
    }
    public function getNumeroTest(){
        return $this->numeroTest;
    }
    public function setTamanoMatriz($tamanoMatriz){
        $this->tamanoMatriz=$tamanoMatriz;
    }
    public function getTamanoMatriz(){
        return $this->tamanoMatriz;
    }
    public function setNumOperaciones($numOperaciones){
        $this->numOperaciones=$numOperaciones;
    }
    public function getNumOperaciones(){
        return $this->numOperaciones;
    }
    public function isItTestTerminado(){
        return $this->itTest>$this->numeroTest;
    }
    public function isItOperacionTerminado(){
        return $this->itOperacion>$this->numOperaciones;
    }
    public function incItTest(){
        $this->itTest++;
    }
    public function incItOperacion(){
        $this->itOperacion++;
    }
    public function getItTest(){
        return $this->itTest;
    }
    public function setItTest($itTest){
        $this->itTest=$itTest;
    }
    public function getItOperacion(){
        return $this->itOperacion;
    }
    public function setItOperacion($itOperacion){
        $this->itOperacion=$itOperacion;
    }
    public function agregarTest($test){
        $this->test[]=$test;
    }          
    public function agregarSentenciaTest($sentencia){
        $this->test[$this->getItTest()-1]->agregarSentencia($sentencia);
}
    public function getVistaMatrizIterando(){
        return $this->test[$this->getItTest()-1]->getVistaMatriz();
    }
    public function ejecutarUltimaSentencia(){
        return $this->test[$this->getItTest()-1]->ejecutarUltimaSentencia();
    }
            

}
