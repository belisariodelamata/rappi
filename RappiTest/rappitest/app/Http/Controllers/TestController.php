<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class TestController extends Controller {

    var $request = null;
    var $entrada = null;
    var $test = null;

    const INICIO_PAGINA = 1;
    const OP_INGRESO_NUMERO_TEST = 2;
    const OP_INGRESO_TAMANO_MATRIZ_Y_OPERACIONES = 3;
    const OP_INGRESO_SENTENCIA = 4;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    function index() {
        try {
            $op = $this->request->input("op");
            $this->entrada = strtoupper(trim($this->request->input("entrada")));
            $this->test = $this->request->session()->get("test", new \App\Http\Bean\ConfTest());
            if (!$op) {
                return view("test");
            }
            switch ($op) {
                case TestController::INICIO_PAGINA:
                    $this->request->session()->start();
                    $this->request->session()->put("paso", 1);
                    $test = new \App\Http\Bean\ConfTest();
                    $this->request->session()->put("test", $test);
                    $this->mensajeIngresoNumeroConfTest();
                    $this->proximaOp(TestController::OP_INGRESO_NUMERO_TEST);
                    break;
                case TestController::OP_INGRESO_NUMERO_TEST:
                    $this->ingresoNumeroDeConfTest();
                    break;
                case TestController::OP_INGRESO_TAMANO_MATRIZ_Y_OPERACIONES:
                    $this->ingresoTamanoMatriz();
                    break;
                case TestController::OP_INGRESO_SENTENCIA:
                    $this->ingresoSentencias();
                    break;
                default;
                    $this->agregarError("Petición No soportada" . ($op ? (":" . $op) : ""));
                    return $this->objetoRespuesta;
            }
            if ($this->test && $this->test->getTamanoMatriz() > 0) {
                $this->agregarPropRespuesta("matriz", $this->test->getVistaMatrizIterando());
            }
            return $this->objetoRespuesta;
        } catch (\Exception $ex) {
            $this->agregarError($ex->getMessage());
            return $this->objetoRespuesta;
        }
    }

    function siguienteEntrada($mensaje) {
        $this->agregarEntrada($mensaje);
    }

    function mensajeIngresoNumeroConfTest() {
        $this->siguienteEntrada("Ingrese el número de Test(T)");
    }

    function ingresoNumeroDeConfTest() {
        if (!$this->entrada) {
            $this->agregarError("Se espera el ingreso del número de Test");
        } else if (!(1 <= $this->entrada && $this->entrada <= 50)) {
            $this->agregarError("El numero de Test debe estar en el rango de 1 a 50 inclusive");
        }
        if (!$this->hayError()) {
            $this->test->setNumeroTest($this->entrada);
            $this->refrescarTestEnSesion();
            $this->proximaOp(TestController::OP_INGRESO_TAMANO_MATRIZ_Y_OPERACIONES);
            ////Aqui debe ser la continuacion del proceso
            $this->mensajeingresoTamanoMatriz();
        } else {
            $this->mensajeIngresoNumeroConfTest();
        }
    }

    function refrescarTestEnSesion() {
        $this->request->session()->put("test", $this->test);
    }

    ////////////
    function mensajeingresoTamanoMatriz() {
        $this->siguienteEntrada("Tamaño de Matriz y Numero de Operaciones (N M)");
    }

    function ingresoTamanoMatriz() {
        if (!$this->entrada) {
            $this->agregarError("Se espera el ingreso del tamaño de la Matriz y Numero De Operaciones");
        } else {
            $numTamanoOperacion = explode(" ", $this->entrada);
            if (count($numTamanoOperacion) != 2) {
                $this->agregarError("Se esperaba dos valores separados por un espacio, encontrados (" . count($numTamanoOperacion) . ")");
            } else if (count($numTamanoOperacion) == 2) {
                if (!\App\Http\Services\RappiValidator::validarNumero($numTamanoOperacion, 0)) {
                    $this->agregarError("Los valores separados por espacios deben corresponder a un número.");
                } else {
                    $n = $numTamanoOperacion[0];
                    $m = $numTamanoOperacion[1];
                    if (!(1 <= $n && $n <= 100)) {
                        $this->agregarError("El Primer Parametro(" . $n . ") debe estar en el rango de 1 a 100 inclusive");
                    }
                    if (!(1 <= $m && $m <= 1000)) {
                        $this->agregarError("El Segundo Parametro(" . $m . ") debe estar en el rango de 1 a 1000 inclusive");
                    }
                    if (!$this->hayError()) {
                        $this->proximaOp(TestController::OP_INGRESO_SENTENCIA);
                        $this->test->setTamanoMatriz($numTamanoOperacion[0]);
                        $this->test->setNumOperaciones($numTamanoOperacion[1]);
                        $this->test->incItTest();
                        $nuevoTest = new \App\Http\Bean\Test($numTamanoOperacion[0]);
                        $this->test->agregarTest($nuevoTest);
                        $this->agregarMensaje("Test " . $this->test->getItTest() . "/" . $this->test->getNumeroTest());
                        $this->test->incItOperacion();
                        ////Aqui debe ser la continuacion del proceso
                        $this->mensajeIngresoSentencias();
                    }
                }
            }
        }
        if (!$this->hayError()) {
            $this->refrescarTestEnSesion();
        } else {
            $this->mensajeingresoTamanoMatriz();
        }
    }

    //////////
    function mensajeIngresoSentencias() {
        $this->siguienteEntrada("Ingrese las sentencias, ya sea QUERY ó UPDATE");
    }

    function ingresoSentencias() {
        if (!$this->entrada) {
            $this->agregarError("Se espera sentencia QUERY ó UPDATE");
        } else {
            $partesSentencia = explode(" ", $this->entrada);
            if (starts_With($this->entrada, "QUERY")) {
                if (count($partesSentencia) != 7) {
                    $this->agregarError("Para la Sentencia QUERY Se esperaban 6 valores más separados por un espacio, encontrados (" . (count($partesSentencia) - 1) . ")");
                } else if (count($partesSentencia) == 7) {
                    if (!\App\Http\Services\RappiValidator::validarNumero($partesSentencia, 1)) {
                        $this->agregarError("Los valores separados por espacios deben corresponder a numeros.");
                    } else {
                        $x1 = $partesSentencia[1];
                        $y1 = $partesSentencia[2];
                        $z1 = $partesSentencia[3];
                        $x2 = $partesSentencia[4];
                        $y2 = $partesSentencia[5];
                        $z2 = $partesSentencia[6];
                        if (!(1 <= $x1 && $x1 <= $x2 && $x2 <= $this->test->getTamanoMatriz())) {
                            $this->agregarError("Los Valores ingresados no cumplen la condicion 1 <= x1 <= x2 <= N.");
                        }
                        if (!(1 <= $y1 && $y1 <= $y2 && $y2 <= $this->test->getTamanoMatriz())) {
                            $this->agregarError("Los Valores ingresados no cumplen la condicion 1 <= y1 <= y2 <= N.");
                        }
                        if (!(1 <= $z1 && $z1 <= $z2 && $z2 <= $this->test->getTamanoMatriz())) {
                            $this->agregarError("Los Valores ingresados no cumplen la condicion 1 <= z1 <= z2 <= N.");
                        }
                        if (!$this->hayError()) {
                            $this->test->agregarSentenciaTest($this->entrada);
                            $ejecutado = $this->test->ejecutarUltimaSentencia();
                            if (!is_null($ejecutado)) {
                                $this->agregarMensaje("Resultado: " . $ejecutado . "  ");
                            }
                            $this->verificarCiclo();
                        }
                    }
                }
            } else if (starts_With($this->entrada, "UPDATE")) {
                if (count($partesSentencia) != 5) {
                    $this->agregarError("Para la Sentencia UPDATE Se esperaban 4 valores separados por un espacio, encontrados (" . (count($partesSentencia) - 1) . ")");
                } else if (count($partesSentencia) == 5) {
                    if (!\App\Http\Services\RappiValidator::validarNumero($partesSentencia, 1)) {
                        $this->agregarError("Los valores separados por espacios deben corresponder a numeros.");
                    } else {
                        $x = $partesSentencia[1];
                        $y = $partesSentencia[2];
                        $z = $partesSentencia[3];
                        $w = $partesSentencia[4];

                        if (!(1 <= $x && $x <= $this->test->getTamanoMatriz())) {
                            $this->agregarError("El valor de x($x) debe estar de 1 a " . $this->test->getTamanoMatriz() . " inclusive.");
                        }
                        if (!(1 <= $y && $y <= $this->test->getTamanoMatriz())) {
                            $this->agregarError("El valor de y($y) debe estar de 1 a " . $this->test->getTamanoMatriz() . " inclusive.");
                        }
                        if (!(1 <= $z && $z <= $this->test->getTamanoMatriz())) {
                            $this->agregarError("El valor de z($z) debe estar de 1 a " . $this->test->getTamanoMatriz() . " inclusive.");
                        }
                        if (!(pow(-10, 9) <= $w && $w <= pow(10, 9))) {
                            $this->agregarError("El valor de w($w) debe estar de -10^9 a 10^9 inclusive.");
                        }

                        if (!$this->hayError()) {
                            $this->test->agregarSentenciaTest($this->entrada);
                            $ejecutado = $this->test->ejecutarUltimaSentencia();
                            if (($ejecutado)) {
                                $this->agregarMensaje("Actualizacion Exitosa");
                            } else {
                                $this->agregarMensaje("No se pudo realizar la actualizacion");
                            }
                            $this->verificarCiclo();
                        }
                    }
                }
            }
        }
        $this->refrescarTestEnSesion();
        if (!$this->hayError()) {
            
        } else {
            $this->mensajeIngresoSentencias();
        }
    }

    private function verificarCiclo() {
        $this->test->incItOperacion();
        if ($this->test->isItOperacionTerminado()) {
            $this->test->incItTest();
            if ($this->test->isItTestTerminado()) {
                $this->agregarEntrada("Test Total Terminado");
            } else {
                $this->agregarMensaje("Test " . $this->test->getItTest() . " iniciado ");
                $this->test->setItOperacion(1);
                $this->agregarMensaje("Operacion " . $this->test->getItOperacion());
                $this->mensajeIngresoSentencias();
            }
        } else {
            $this->agregarMensaje("Operacion " . $this->test->getItOperacion());
            $this->mensajeIngresoSentencias();
        }
    }

}
