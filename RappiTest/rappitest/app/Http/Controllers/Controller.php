<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController {

    use AuthorizesRequests,
        AuthorizesResources,
        DispatchesJobs,
        ValidatesRequests;

    var $request;
    var $objetoRespuesta = array();

    public function agregarPropRespuesta($prop, $value) {
        $this->objetoRespuesta[$prop] = $value;
    }

    public function agregarListaValores($prop, $valor) {
        if (!key_exists($prop, $this->objetoRespuesta)) {
            $this->objetoRespuesta[$prop] = array();
        }
        $this->objetoRespuesta[$prop][] = $valor;
    }

    public function agregarMensaje($mensaje) {
        $this->agregarListaValores("msj", $mensaje);
    }

    public function agregarEntrada($entrada) {
        $this->agregarListaValores("entrada", $entrada);
    }

    public function agregarError($error) {
        $this->agregarListaValores("error", $error);
    }

    public function agregarObj($objeto) {
        $this->agregarListaValores("obj", $objeto);
    }

    public function proximaOp($op) {
        $this->agregarPropRespuesta("op", $op);
    }

    public function hayError() {
        return key_exists("error", $this->objetoRespuesta);
    }

}
